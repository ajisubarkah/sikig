package goodsinventory.utility;

import java.math.BigInteger;
import java.security.MessageDigest;
import java.security.NoSuchAlgorithmException;
import java.text.NumberFormat;
import java.util.Locale;
import java.util.StringTokenizer;
import javax.swing.JTable;

public class FunctionUtility {

    public static String md5(String input) {
        try {
            MessageDigest md = MessageDigest.getInstance("MD5");

            byte[] messageDigest = md.digest(input.getBytes());

            BigInteger no = new BigInteger(1, messageDigest);

            String hashtext = no.toString(16);
            while (hashtext.length() < 32) {
                hashtext = "0" + hashtext;
            }
            return hashtext;
        } catch (NoSuchAlgorithmException e) {
            throw new RuntimeException(e);
        }
    }
    
    public void setTableSize(int n, int arr[], JTable table){
        table.setDefaultEditor(Object.class, null);
        for(int i = 0; i < n; i++){
            table.getColumnModel().getColumn(i).setMinWidth(arr[i]);
        }
        
        ((javax.swing.table.DefaultTableCellRenderer)table.getTableHeader().getDefaultRenderer()).setHorizontalAlignment(0);
        
//        javax.swing.table.DefaultTableCellRenderer centerRenderer = new javax.swing.table.DefaultTableCellRenderer();
//        centerRenderer.setHorizontalAlignment(javax.swing.JLabel.CENTER);
//        for (int i = 0; i < n; i++) {
//            table.getColumnModel().getColumn(i).setCellRenderer(centerRenderer);
//        }
    }
    
    public String moneyFormat(int formatMoney){
        String money = NumberFormat.getNumberInstance(Locale.ENGLISH).format(formatMoney);
        StringTokenizer token = new StringTokenizer(money, ".");
        money = token.nextToken();
        money = money.replace(",",".");
        return money;
    }

    public String moneyFormat(String str){
        char[] arr = str.toCharArray();
        String backStr = "";
        int i = 0;
        while(i < str.length()){
            if(arr[i] >= 48 && arr[i] <= 57){
                backStr = backStr+arr[i];
            }
            if(arr[i] == 44){
                    i += 2;
            }
            i++;
        }
        return backStr;
    }
}
