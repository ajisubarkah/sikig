package goodsinventory.utility;

import java.sql.*;

public class ConnectionDatabase {

    private final String dbName = "goodsinventory";
    private final String dbUser = "subarkah";
    private final String dbPass = "gausah";
    private static Connection connection = null;
    private static ResultSet resultSet;
    private static Statement statement;

    public void openConection() {
        try {
            Class.forName("com.mysql.jdbc.Driver");
            connection = DriverManager.getConnection("jdbc:mysql://localhost/" + dbName, dbUser, dbPass);
            statement = (Statement) connection.createStatement();
        } catch (ClassNotFoundException | SQLException ex) {
            javax.swing.JOptionPane.showMessageDialog(null, ex, "Error", javax.swing.JOptionPane.ERROR_MESSAGE);
        }
    }

    public void closeConnection() {
        try {
            connection.close();
        } catch (SQLException ex) {
            javax.swing.JOptionPane.showMessageDialog(null, ex, "Error", javax.swing.JOptionPane.ERROR_MESSAGE);
        }
    }

    public String getDataString(String query) {
        try {
            openConection();
            resultSet = statement.executeQuery(query);
            String data = null;
            if (resultSet.next()) {
                data = resultSet.getString(1);
            }
            closeConnection();
            return data;
        } catch (SQLException ex) {
            javax.swing.JOptionPane.showMessageDialog(null, ex, "Error", javax.swing.JOptionPane.ERROR_MESSAGE);
            closeConnection();
            return null;
        }
    }

    public Object[] getColoumnName(String query) {
        try {
            openConection();
            resultSet = statement.executeQuery(query);
            int coloumnSize = resultSet.getMetaData().getColumnCount();

            String[] coloumnName = new String[coloumnSize];

            for (int i = 0; i < coloumnName.length; i++) {
                coloumnName[i] = resultSet.getMetaData().getColumnName(i + 1);
            }
            connection.close();
            return coloumnName;
        } catch (SQLException ex) {
            javax.swing.JOptionPane.showMessageDialog(null, ex, "Error", javax.swing.JOptionPane.ERROR_MESSAGE);
            closeConnection();
            return null;
        }
    }

    public int getCountData(String query) {
        try {
            openConection();
            resultSet = statement.executeQuery(query);
            int count = 0;
            while (resultSet.next()) {
                count++;
            }
            closeConnection();
            return count;
        } catch (SQLException ex) {
            javax.swing.JOptionPane.showMessageDialog(null, ex, "Error", javax.swing.JOptionPane.ERROR_MESSAGE);
            closeConnection();
            return 0;
        }

    }

    public Object[][] getAllDataTable(String query) {
        try {
            openConection();
            resultSet = statement.executeQuery(query);
            int coloumnSize = resultSet.getMetaData().getColumnCount();
            int count = 0;
            String[] coloumnName = new String[coloumnSize];

            java.util.ArrayList<java.util.ArrayList<Object>> coloumnData = new java.util.ArrayList<>();

            for (int i = 0; i < coloumnName.length; i++) {
                coloumnName[i] = resultSet.getMetaData().getColumnName(i + 1);
                coloumnData.add(new java.util.ArrayList<>());
            }

            while (resultSet.next()) {
                for (int i = 0; i < coloumnSize; i++) {
                    coloumnData.get(i).add(resultSet.getObject(i + 1));
                }
                count++;
            }

            Object[][] coloumnDataArray = new Object[count][coloumnSize];
            for (int i = 0; i < count; i++) {
                for (int j = 0; j < coloumnSize; j++) {
                    coloumnDataArray[i][j] = coloumnData.get(j).get(i);
                }
            }
            connection.close();
            return coloumnDataArray;
        } catch (SQLException ex) {
            javax.swing.JOptionPane.showMessageDialog(null, ex, "Error", javax.swing.JOptionPane.ERROR_MESSAGE);
            closeConnection();
            return null;
        }
    }

    public void exQuery(String query) {
        try {
            openConection();
            statement.executeUpdate(query);
            connection.close();
        } catch (SQLException ex) {
            closeConnection();
            javax.swing.JOptionPane.showMessageDialog(null, ex, "Error", javax.swing.JOptionPane.ERROR_MESSAGE);
        }
    }
}
