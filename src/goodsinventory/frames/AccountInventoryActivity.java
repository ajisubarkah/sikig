package goodsinventory.frames;

import goodsinventory.utility.ConnectionDatabase;
import goodsinventory.utility.FunctionUtility;
import java.awt.Frame;
import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;
import javax.swing.JDialog;

public final class AccountInventoryActivity extends JDialog implements ActionListener {

    private boolean checkExitStatus = false, checkOption = false;
    private String id, name, username, password, status, query;
    ConnectionDatabase connection = new ConnectionDatabase();

    public AccountInventoryActivity(Frame parent, String title) {
        super(parent, title, true);
        initComponents();
        setResizable(false);
        setLocationRelativeTo(null);
    }

    public AccountInventoryActivity(Frame parent, String title, String data[]) {
        super(parent, title, true);
        checkOption = true;
        id = data[0];
        name = data[3];
        username = data[1];
        password = data[2];
        status = data[4];
        initComponents();
        setUpdateData();
        setResizable(false);
        setLocationRelativeTo(null);
    }

    public void setUpdateData() {
        jName.setText(name);
        jUsername.setText(username);
        if (status.equals("true")) {
            jComboBox1.setSelectedIndex(1);
        } else {
            jComboBox1.setSelectedIndex(0);
        }
    }

    @Override
    public void actionPerformed(ActionEvent ae) {
        Object sourceObject = ae.getSource();

        checkExitStatus = sourceObject == btnOk;

        if (checkOption) {
            if (checkExitStatus && jPassword.getText().equals("")) {
                query = "UPDATE account SET username='" + jUsername.getText() + "', name='"
                        + jName.getText() + "', stats='" + jComboBox1.getSelectedIndex() + "' WHERE id_account=" + id;
                connection.exQuery(query);
            } else if (checkExitStatus) {
                query = "UPDATE account SET username='" + jUsername.getText() + "', password='"
                        + FunctionUtility.md5(jPassword.getText()) + "', name='" + jName.getText() + "', stats='"
                        + jComboBox1.getSelectedIndex() + "' WHERE account.id_account=" + id;
                connection.exQuery(query);
            }
        } else if (checkExitStatus) {
            query = "INSERT INTO account VALUES (NULL,'" + jUsername.getText() + "','" + FunctionUtility.md5(jPassword.getText()) + "','"
                    + jName.getText() + "','" + jComboBox1.getSelectedIndex() + "')";
            connection.exQuery(query);
        }

        dispose();
    }

    public boolean run() {
        this.setVisible(true);
        return checkExitStatus;
    }

    private void initComponents() {
        bindingGroup = new org.jdesktop.beansbinding.BindingGroup();
        jLabel1 = new javax.swing.JLabel();
        jLabelHeader = new javax.swing.JLabel();
        jName = new javax.swing.JTextField();
        jLabel2 = new javax.swing.JLabel();
        jUsername = new javax.swing.JTextField();
        jLabel3 = new javax.swing.JLabel();
        jPassword = new javax.swing.JTextField();
        jLabel4 = new javax.swing.JLabel();
        jComboBox1 = new javax.swing.JComboBox<>();
        btnCancel = new javax.swing.JButton();
        btnOk = new javax.swing.JButton();

        jLabel1.setText("Nama       :");

        jLabelHeader.setFont(new java.awt.Font("Roboto", 1, 24));
        jLabelHeader.setHorizontalAlignment(javax.swing.SwingConstants.CENTER);
        jLabelHeader.setText("ACCOUNT MANAGER");

        jLabel2.setText("Username :");

        jLabel3.setText("Password :");

        jLabel4.setText("Status        :");

        jComboBox1.setModel(new javax.swing.DefaultComboBoxModel<>(new String[]{"Deactive", "Active"}));

        btnCancel.setText("Cancel");
        btnCancel.setActionCommand("btnCancel");
        btnCancel.addActionListener(this);

        btnOk.setText("Ok");
        btnOk.setActionCommand("btnOk");
        btnOk.addActionListener(this);
        org.jdesktop.beansbinding.Binding binding = org.jdesktop.beansbinding.Bindings.createAutoBinding(org.jdesktop.beansbinding.AutoBinding.UpdateStrategy.READ_WRITE, btnOk, org.jdesktop.beansbinding.ObjectProperty.create(), btnOk, org.jdesktop.beansbinding.BeanProperty.create("actionCommand"));
        bindingGroup.addBinding(binding);

        javax.swing.GroupLayout layout = new javax.swing.GroupLayout(getContentPane());
        getContentPane().setLayout(layout);
        layout.setHorizontalGroup(
                layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
                        .addGroup(layout.createSequentialGroup()
                                .addContainerGap()
                                .addGroup(layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
                                        .addComponent(jLabelHeader, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, Short.MAX_VALUE)
                                        .addGroup(layout.createSequentialGroup()
                                                .addGroup(layout.createParallelGroup(javax.swing.GroupLayout.Alignment.TRAILING, false)
                                                        .addComponent(jLabel1, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, Short.MAX_VALUE)
                                                        .addComponent(jLabel2, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, Short.MAX_VALUE))
                                                .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.RELATED)
                                                .addGroup(layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
                                                        .addGroup(layout.createSequentialGroup()
                                                                .addComponent(jUsername, javax.swing.GroupLayout.PREFERRED_SIZE, 216, javax.swing.GroupLayout.PREFERRED_SIZE)
                                                                .addGap(0, 0, Short.MAX_VALUE))
                                                        .addComponent(jName, javax.swing.GroupLayout.Alignment.TRAILING, javax.swing.GroupLayout.PREFERRED_SIZE, 216, javax.swing.GroupLayout.PREFERRED_SIZE)))
                                        .addGroup(javax.swing.GroupLayout.Alignment.TRAILING, layout.createSequentialGroup()
                                                .addGap(0, 0, Short.MAX_VALUE)
                                                .addComponent(btnOk, javax.swing.GroupLayout.PREFERRED_SIZE, 65, javax.swing.GroupLayout.PREFERRED_SIZE)
                                                .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.UNRELATED)
                                                .addComponent(btnCancel, javax.swing.GroupLayout.PREFERRED_SIZE, 65, javax.swing.GroupLayout.PREFERRED_SIZE))
                                        .addGroup(layout.createSequentialGroup()
                                                .addGroup(layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
                                                        .addGroup(layout.createSequentialGroup()
                                                                .addComponent(jLabel3)
                                                                .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.RELATED)
                                                                .addComponent(jPassword, javax.swing.GroupLayout.PREFERRED_SIZE, 216, javax.swing.GroupLayout.PREFERRED_SIZE))
                                                        .addGroup(layout.createSequentialGroup()
                                                                .addComponent(jLabel4)
                                                                .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.RELATED)
                                                                .addComponent(jComboBox1, javax.swing.GroupLayout.PREFERRED_SIZE, 216, javax.swing.GroupLayout.PREFERRED_SIZE)))
                                                .addGap(0, 0, Short.MAX_VALUE)))
                                .addContainerGap())
        );
        layout.setVerticalGroup(
                layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
                        .addGroup(layout.createSequentialGroup()
                                .addContainerGap()
                                .addComponent(jLabelHeader, javax.swing.GroupLayout.PREFERRED_SIZE, 40, javax.swing.GroupLayout.PREFERRED_SIZE)
                                .addGap(31, 31, 31)
                                .addGroup(layout.createParallelGroup(javax.swing.GroupLayout.Alignment.BASELINE)
                                        .addComponent(jLabel1)
                                        .addComponent(jName, javax.swing.GroupLayout.PREFERRED_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.PREFERRED_SIZE))
                                .addGap(18, 18, 18)
                                .addGroup(layout.createParallelGroup(javax.swing.GroupLayout.Alignment.BASELINE)
                                        .addComponent(jLabel2)
                                        .addComponent(jUsername, javax.swing.GroupLayout.PREFERRED_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.PREFERRED_SIZE))
                                .addGap(18, 18, 18)
                                .addGroup(layout.createParallelGroup(javax.swing.GroupLayout.Alignment.BASELINE)
                                        .addComponent(jLabel3)
                                        .addComponent(jPassword, javax.swing.GroupLayout.PREFERRED_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.PREFERRED_SIZE))
                                .addGap(18, 18, 18)
                                .addGroup(layout.createParallelGroup(javax.swing.GroupLayout.Alignment.BASELINE)
                                        .addComponent(jLabel4)
                                        .addComponent(jComboBox1, javax.swing.GroupLayout.PREFERRED_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.PREFERRED_SIZE))
                                .addGap(18, 18, 18)
                                .addGroup(layout.createParallelGroup(javax.swing.GroupLayout.Alignment.BASELINE)
                                        .addComponent(btnCancel)
                                        .addComponent(btnOk))
                                .addContainerGap(javax.swing.GroupLayout.DEFAULT_SIZE, Short.MAX_VALUE))
        );

        pack();
    }

    private javax.swing.JButton btnCancel;
    private javax.swing.JButton btnOk;
    private javax.swing.JComboBox<String> jComboBox1;
    private javax.swing.JLabel jLabel1;
    private javax.swing.JLabel jLabel2;
    private javax.swing.JLabel jLabel3;
    private javax.swing.JLabel jLabel4;
    private javax.swing.JLabel jLabelHeader;
    private javax.swing.JTextField jName;
    private javax.swing.JTextField jUsername;
    private javax.swing.JTextField jPassword;
    private org.jdesktop.beansbinding.BindingGroup bindingGroup;
}
