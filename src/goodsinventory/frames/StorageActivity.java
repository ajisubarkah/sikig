package goodsinventory.frames;

import goodsinventory.utility.ConnectionDatabase;
import goodsinventory.utility.FunctionUtility;
import javax.swing.JOptionPane;

public class StorageActivity extends javax.swing.JFrame {

    private final ConnectionDatabase connectionDB;
    private final FunctionUtility function;
    private String query;
    private static Object[] dataColoumn;
    private static Object[][] dataRow;

    public StorageActivity() {
        connectionDB = new ConnectionDatabase();
        function = new FunctionUtility();
        setExtendedState(MAXIMIZED_BOTH);
        initComponents();
        setTitle("Panel - Administration");
        setVisible(true);
        setTable(-1);
    }

    // <editor-fold defaultstate="collapsed" desc="Generated Code">//GEN-BEGIN:initComponents
    private void initComponents() {

        jMenuBar1 = new javax.swing.JMenuBar();
        jMenu1 = new javax.swing.JMenu();
        jMenu2 = new javax.swing.JMenu();
        jTabbedPane = new javax.swing.JTabbedPane();
        panelAccount = new java.awt.Panel();
        jScrollPane2 = new javax.swing.JScrollPane();
        jTableAccount = new javax.swing.JTable();
        jButtonCreateAccount = new javax.swing.JButton();
        jButtonUpdateAccount = new javax.swing.JButton();
        jButtonDeleteAccount = new javax.swing.JButton();
        panel1 = new java.awt.Panel();
        jScrollPane1 = new javax.swing.JScrollPane();
        jTableGoods = new javax.swing.JTable();
        jButtonCreateGoods = new javax.swing.JButton();
        jButtonUpdateGoods = new javax.swing.JButton();
        jButtonDeleteGoods = new javax.swing.JButton();
        jPanel1 = new javax.swing.JPanel();
        jScrollPane3 = new javax.swing.JScrollPane();
        jTableReport = new javax.swing.JTable();
        jLabel1 = new javax.swing.JLabel();
        jMenuBar2 = new javax.swing.JMenuBar();
        jMenu3 = new javax.swing.JMenu();
        jMenuItem1 = new javax.swing.JMenuItem();

        jMenu1.setText("File");
        jMenuBar1.add(jMenu1);

        jMenu2.setText("Edit");
        jMenuBar1.add(jMenu2);

        setDefaultCloseOperation(javax.swing.WindowConstants.EXIT_ON_CLOSE);

        jTabbedPane.setCursor(new java.awt.Cursor(java.awt.Cursor.DEFAULT_CURSOR));

        jTableAccount.setModel(new javax.swing.table.DefaultTableModel(
            new Object [][] {

            },
            new String [] {

            }
        ));
        jTableAccount.setCursor(new java.awt.Cursor(java.awt.Cursor.DEFAULT_CURSOR));
        jTableAccount.getTableHeader().setReorderingAllowed(false);
        jScrollPane2.setViewportView(jTableAccount);

        jButtonCreateAccount.setText("Add");
        jButtonCreateAccount.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                jButtonCreateAccountActionPerformed(evt);
            }
        });

        jButtonUpdateAccount.setText("Edit");
        jButtonUpdateAccount.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                jButtonUpdateAccountActionPerformed(evt);
            }
        });

        jButtonDeleteAccount.setText("Delete");
        jButtonDeleteAccount.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                jButtonDeleteAccountActionPerformed(evt);
            }
        });

        javax.swing.GroupLayout panelAccountLayout = new javax.swing.GroupLayout(panelAccount);
        panelAccount.setLayout(panelAccountLayout);
        panelAccountLayout.setHorizontalGroup(
            panelAccountLayout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
            .addGroup(panelAccountLayout.createSequentialGroup()
                .addContainerGap()
                .addComponent(jScrollPane2, javax.swing.GroupLayout.DEFAULT_SIZE, 1113, Short.MAX_VALUE)
                .addGap(18, 18, 18)
                .addGroup(panelAccountLayout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
                    .addComponent(jButtonCreateAccount, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, Short.MAX_VALUE)
                    .addComponent(jButtonDeleteAccount, javax.swing.GroupLayout.DEFAULT_SIZE, 161, Short.MAX_VALUE)
                    .addComponent(jButtonUpdateAccount, javax.swing.GroupLayout.Alignment.TRAILING, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, Short.MAX_VALUE))
                .addContainerGap())
        );
        panelAccountLayout.setVerticalGroup(
            panelAccountLayout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
            .addGroup(panelAccountLayout.createSequentialGroup()
                .addContainerGap()
                .addGroup(panelAccountLayout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
                    .addComponent(jScrollPane2, javax.swing.GroupLayout.PREFERRED_SIZE, 0, Short.MAX_VALUE)
                    .addGroup(panelAccountLayout.createSequentialGroup()
                        .addComponent(jButtonCreateAccount, javax.swing.GroupLayout.PREFERRED_SIZE, 50, javax.swing.GroupLayout.PREFERRED_SIZE)
                        .addGap(18, 18, 18)
                        .addComponent(jButtonUpdateAccount, javax.swing.GroupLayout.PREFERRED_SIZE, 50, javax.swing.GroupLayout.PREFERRED_SIZE)
                        .addGap(18, 18, 18)
                        .addComponent(jButtonDeleteAccount, javax.swing.GroupLayout.PREFERRED_SIZE, 50, javax.swing.GroupLayout.PREFERRED_SIZE)
                        .addGap(0, 62, Short.MAX_VALUE)))
                .addContainerGap())
        );

        jTabbedPane.addTab("Account", panelAccount);

        jTableGoods.setModel(new javax.swing.table.DefaultTableModel(
            new Object [][] {

            },
            new String [] {

            }
        ));
        jScrollPane1.setViewportView(jTableGoods);

        jButtonCreateGoods.setText("Add");
        jButtonCreateGoods.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                jButtonCreateGoodsActionPerformed(evt);
            }
        });

        jButtonUpdateGoods.setText("Edit");
        jButtonUpdateGoods.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                jButtonUpdateGoodsActionPerformed(evt);
            }
        });

        jButtonDeleteGoods.setText("Delete");
        jButtonDeleteGoods.setToolTipText("");
        jButtonDeleteGoods.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                jButtonDeleteGoodsActionPerformed(evt);
            }
        });

        javax.swing.GroupLayout panel1Layout = new javax.swing.GroupLayout(panel1);
        panel1.setLayout(panel1Layout);
        panel1Layout.setHorizontalGroup(
            panel1Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
            .addGroup(panel1Layout.createSequentialGroup()
                .addContainerGap()
                .addComponent(jScrollPane1, javax.swing.GroupLayout.DEFAULT_SIZE, 1113, Short.MAX_VALUE)
                .addGap(18, 18, 18)
                .addGroup(panel1Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING, false)
                    .addComponent(jButtonUpdateGoods, javax.swing.GroupLayout.DEFAULT_SIZE, 161, Short.MAX_VALUE)
                    .addComponent(jButtonCreateGoods, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, Short.MAX_VALUE)
                    .addComponent(jButtonDeleteGoods, javax.swing.GroupLayout.DEFAULT_SIZE, 161, Short.MAX_VALUE))
                .addContainerGap())
        );
        panel1Layout.setVerticalGroup(
            panel1Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
            .addGroup(panel1Layout.createSequentialGroup()
                .addContainerGap()
                .addGroup(panel1Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
                    .addComponent(jScrollPane1, javax.swing.GroupLayout.PREFERRED_SIZE, 0, Short.MAX_VALUE)
                    .addGroup(panel1Layout.createSequentialGroup()
                        .addComponent(jButtonCreateGoods, javax.swing.GroupLayout.PREFERRED_SIZE, 50, javax.swing.GroupLayout.PREFERRED_SIZE)
                        .addGap(18, 18, 18)
                        .addComponent(jButtonUpdateGoods, javax.swing.GroupLayout.PREFERRED_SIZE, 50, javax.swing.GroupLayout.PREFERRED_SIZE)
                        .addGap(18, 18, 18)
                        .addComponent(jButtonDeleteGoods, javax.swing.GroupLayout.PREFERRED_SIZE, 50, javax.swing.GroupLayout.PREFERRED_SIZE)
                        .addGap(0, 62, Short.MAX_VALUE)))
                .addContainerGap())
        );

        jTabbedPane.addTab("Goods", panel1);

        jTableReport.setModel(new javax.swing.table.DefaultTableModel(
            new Object [][] {

            },
            new String [] {

            }
        ));
        jScrollPane3.setViewportView(jTableReport);

        javax.swing.GroupLayout jPanel1Layout = new javax.swing.GroupLayout(jPanel1);
        jPanel1.setLayout(jPanel1Layout);
        jPanel1Layout.setHorizontalGroup(
            jPanel1Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
            .addGroup(jPanel1Layout.createSequentialGroup()
                .addContainerGap()
                .addComponent(jScrollPane3, javax.swing.GroupLayout.DEFAULT_SIZE, 1292, Short.MAX_VALUE)
                .addContainerGap())
        );
        jPanel1Layout.setVerticalGroup(
            jPanel1Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
            .addGroup(jPanel1Layout.createSequentialGroup()
                .addContainerGap()
                .addComponent(jScrollPane3, javax.swing.GroupLayout.PREFERRED_SIZE, 0, Short.MAX_VALUE)
                .addContainerGap())
        );

        jTabbedPane.addTab("Report Transaction", jPanel1);

        jLabel1.setFont(new java.awt.Font("Liberation Mono", 1, 48)); // NOI18N
        jLabel1.setHorizontalAlignment(javax.swing.SwingConstants.CENTER);
        jLabel1.setText("SIKIG INVENTORY");

        jMenu3.setText("File");

        jMenuItem1.setText("Logout");
        jMenuItem1.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                jMenuItem1ActionPerformed(evt);
            }
        });
        jMenu3.add(jMenuItem1);

        jMenuBar2.add(jMenu3);

        setJMenuBar(jMenuBar2);

        javax.swing.GroupLayout layout = new javax.swing.GroupLayout(getContentPane());
        getContentPane().setLayout(layout);
        layout.setHorizontalGroup(
            layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
            .addGroup(layout.createSequentialGroup()
                .addContainerGap()
                .addGroup(layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
                    .addComponent(jTabbedPane)
                    .addComponent(jLabel1, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, Short.MAX_VALUE))
                .addContainerGap())
        );
        layout.setVerticalGroup(
            layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
            .addGroup(layout.createSequentialGroup()
                .addContainerGap()
                .addComponent(jLabel1)
                .addGap(18, 18, 18)
                .addComponent(jTabbedPane)
                .addContainerGap())
        );

        pack();
    }// </editor-fold>//GEN-END:initComponents

    private void jButtonUpdateAccountActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_jButtonUpdateAccountActionPerformed
        if (jTableAccount.getSelectedRowCount() < 1) {
            javax.swing.JOptionPane.showMessageDialog(null, "Please select row!", "Warning", javax.swing.JOptionPane.WARNING_MESSAGE);
        } else if (jTableAccount.getSelectedRowCount() > 1) {
            javax.swing.JOptionPane.showMessageDialog(null, "Please select Only one row!", "Warning", javax.swing.JOptionPane.WARNING_MESSAGE);
        } else {
            int i = jTableAccount.getSelectedRow();
            String data[] = {
                jTableAccount.getValueAt(i, 0).toString(),
                jTableAccount.getValueAt(i, 1).toString(),
                jTableAccount.getValueAt(i, 2).toString(),
                jTableAccount.getValueAt(i, 3).toString(),
                jTableAccount.getValueAt(i, 4).toString()
            };
            AccountInventoryActivity update = new AccountInventoryActivity(this, "Update Data", data);
            boolean status = update.run();
            if (status) {
                JOptionPane.showMessageDialog(this, "SUCCESS", "Information", JOptionPane.INFORMATION_MESSAGE);
                setTable(0);
            }
        }
    }//GEN-LAST:event_jButtonUpdateAccountActionPerformed

    private void jButtonCreateAccountActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_jButtonCreateAccountActionPerformed
        AccountInventoryActivity add = new AccountInventoryActivity(this, "New Account");
        boolean status = add.run();
        if (status) {
            JOptionPane.showMessageDialog(this, "SUCCESS", "Information", JOptionPane.INFORMATION_MESSAGE);
            setTable(0);
        }
    }//GEN-LAST:event_jButtonCreateAccountActionPerformed

    private void jButtonDeleteAccountActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_jButtonDeleteAccountActionPerformed
        if (jTableAccount.getSelectedRowCount() < 1) {
            javax.swing.JOptionPane.showMessageDialog(null, "Please select row to Delete!", "Warning", javax.swing.JOptionPane.WARNING_MESSAGE);
        } else if (jTableAccount.getSelectedRowCount() > 1) {
            javax.swing.JOptionPane.showMessageDialog(null, "Please select Only one Row to Delete!", "Warning", javax.swing.JOptionPane.WARNING_MESSAGE);
        } else {
            int row = jTableAccount.getSelectedRow();
            int returnVal = javax.swing.JOptionPane.showConfirmDialog(null, "Are you sure to Delete Permanently data ?", "Confirm Delete", javax.swing.JOptionPane.YES_NO_OPTION);
            if (returnVal == javax.swing.JOptionPane.YES_OPTION) {
                query = "DELETE FROM account WHERE id_account=" + jTableAccount.getValueAt(row, 0);
                connectionDB.exQuery(query);
                javax.swing.JOptionPane.showMessageDialog(null, "Success Delete!", "Information", javax.swing.JOptionPane.INFORMATION_MESSAGE);
                setTable(0);
            }
        }
    }//GEN-LAST:event_jButtonDeleteAccountActionPerformed

    private void jButtonCreateGoodsActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_jButtonCreateGoodsActionPerformed
        GoodsInventoryActivity add = new GoodsInventoryActivity(this, "New Goods");
        boolean status = add.run();
        if (status) {
            JOptionPane.showMessageDialog(this, "SUCCESS", "Information", JOptionPane.INFORMATION_MESSAGE);
            setTable(1);
        }
    }//GEN-LAST:event_jButtonCreateGoodsActionPerformed

    private void jButtonUpdateGoodsActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_jButtonUpdateGoodsActionPerformed
        if (jTableGoods.getSelectedRowCount() < 1) {
            javax.swing.JOptionPane.showMessageDialog(null, "Please select row!", "Warning", javax.swing.JOptionPane.WARNING_MESSAGE);
        } else if (jTableGoods.getSelectedRowCount() > 1) {
            javax.swing.JOptionPane.showMessageDialog(null, "Please select Only one row!", "Warning", javax.swing.JOptionPane.WARNING_MESSAGE);
        } else {
            int i = jTableGoods.getSelectedRow();
            String data[] = {
                jTableGoods.getValueAt(i, 0).toString(),
                jTableGoods.getValueAt(i, 1).toString(),
                jTableGoods.getValueAt(i, 2).toString(),
                jTableGoods.getValueAt(i, 3).toString(),
                jTableGoods.getValueAt(i, 4).toString()
            };
            GoodsInventoryActivity update = new GoodsInventoryActivity(this, "Update Data", data);
            boolean status = update.run();
            if (status) {
                JOptionPane.showMessageDialog(this, "SUCCESS", "Information", JOptionPane.INFORMATION_MESSAGE);
                setTable(1);
            }
        }
    }//GEN-LAST:event_jButtonUpdateGoodsActionPerformed

    private void jButtonDeleteGoodsActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_jButtonDeleteGoodsActionPerformed
        if (jTableGoods.getSelectedRowCount() < 1) {
            javax.swing.JOptionPane.showMessageDialog(null, "Please select row to Delete!", "Warning", javax.swing.JOptionPane.WARNING_MESSAGE);
        } else if (jTableGoods.getSelectedRowCount() > 1) {
            javax.swing.JOptionPane.showMessageDialog(null, "Please select Only one Row to Delete!", "Warning", javax.swing.JOptionPane.WARNING_MESSAGE);
        } else {
            int row = jTableGoods.getSelectedRow();
            int returnVal = javax.swing.JOptionPane.showConfirmDialog(null, "Are you sure to Delete Permanently data ?", "Confirm Delete", javax.swing.JOptionPane.YES_NO_OPTION);
            if (returnVal == javax.swing.JOptionPane.YES_OPTION) {
                query = "DELETE FROM goods WHERE id_goods=" + jTableGoods.getValueAt(row, 0);
                connectionDB.exQuery(query);
                javax.swing.JOptionPane.showMessageDialog(null, "Success Delete!", "Information", javax.swing.JOptionPane.INFORMATION_MESSAGE);
                setTable(1);
            }
        }
    }//GEN-LAST:event_jButtonDeleteGoodsActionPerformed

    private void jMenuItem1ActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_jMenuItem1ActionPerformed
        dispose();
        new LoginActivity();
    }//GEN-LAST:event_jMenuItem1ActionPerformed

    private void setTable(int index) {
        if (index == 0 || index == -1) {
            query = "SELECT * FROM account";
            dataColoumn = connectionDB.getColoumnName(query);
            dataRow = connectionDB.getAllDataTable(query);
            jTableAccount.setModel(new javax.swing.table.DefaultTableModel(dataRow, dataColoumn));
            int arr[] = {100, 250, 300, 350, 100};
            function.setTableSize(5, arr, jTableAccount);
        }
        if (index == 1 || index == -1) {
            query = "SELECT * FROM goods";
            dataColoumn = connectionDB.getColoumnName(query);
            dataRow = connectionDB.getAllDataTable(query);
            jTableGoods.setModel(new javax.swing.table.DefaultTableModel(dataRow, dataColoumn));
            int arr[] = {100, 450, 200, 200, 100};
            function.setTableSize(5, arr, jTableGoods);
        }
        if (index == 2 || index == -1) {
            query = "SELECT * FROM transaction";
            dataColoumn = connectionDB.getColoumnName(query);
            dataRow = connectionDB.getAllDataTable(query);
            jTableReport.setModel(new javax.swing.table.DefaultTableModel(dataRow, dataColoumn));
            int arr[] = {150, 100, 200, 200};
            function.setTableSize(4, arr, jTableReport);
        }
    }

    // Variables declaration - do not modify//GEN-BEGIN:variables
    private javax.swing.JButton jButtonCreateAccount;
    private javax.swing.JButton jButtonCreateGoods;
    private javax.swing.JButton jButtonDeleteAccount;
    private javax.swing.JButton jButtonDeleteGoods;
    private javax.swing.JButton jButtonUpdateAccount;
    private javax.swing.JButton jButtonUpdateGoods;
    private javax.swing.JLabel jLabel1;
    private javax.swing.JMenu jMenu1;
    private javax.swing.JMenu jMenu2;
    private javax.swing.JMenu jMenu3;
    private javax.swing.JMenuBar jMenuBar1;
    private javax.swing.JMenuBar jMenuBar2;
    private javax.swing.JMenuItem jMenuItem1;
    private javax.swing.JPanel jPanel1;
    private javax.swing.JScrollPane jScrollPane1;
    private javax.swing.JScrollPane jScrollPane2;
    private javax.swing.JScrollPane jScrollPane3;
    private javax.swing.JTabbedPane jTabbedPane;
    private javax.swing.JTable jTableAccount;
    private javax.swing.JTable jTableGoods;
    private javax.swing.JTable jTableReport;
    private java.awt.Panel panel1;
    private java.awt.Panel panelAccount;
    // End of variables declaration//GEN-END:variables
}
