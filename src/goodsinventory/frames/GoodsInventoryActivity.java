package goodsinventory.frames;

import goodsinventory.GoodsInventory;
import goodsinventory.utility.ConnectionDatabase;
import goodsinventory.utility.FunctionUtility;
import java.awt.Frame;
import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;
import javax.swing.JDialog;

public class GoodsInventoryActivity extends JDialog implements ActionListener {

    private boolean checkExitStatus = false, checkOption = false;
    private String id, name, hpb, hpp, amount, query;
    ConnectionDatabase connection = new ConnectionDatabase();

    public GoodsInventoryActivity(Frame parent, String title) {
        super(parent, title, true);
        initComponents();
        setResizable(false);
        setLocationRelativeTo(null);
    }

    public GoodsInventoryActivity(Frame parent, String title, String data[]) {
        super(parent, title, true);
        checkOption = true;
        id = data[0];
        name = data[1];
        hpb = data[2];
        hpp = data[3];
        amount = data[4];
        initComponents();
        setUpdateData();
        setResizable(false);
        setLocationRelativeTo(null);
    }

    public void setUpdateData() {
        jName.setText(name);
        jHPB.setText(hpb);
        jHPP.setText(hpp);
        jStock.setText(amount);
    }

    @Override
    public void actionPerformed(ActionEvent ae) {
        Object sourceObject = ae.getSource();

        checkExitStatus = sourceObject == jButtonOk;

        if (checkOption && checkExitStatus) {
            query = "UPDATE goods SET name='" + jName.getText() + "', hpb='" + jHPB.getText() + "', hpp='"
                    + jHPP.getText() + "', amount='" + jStock.getText() + "' WHERE id_goods=" + id;
            connection.exQuery(query);
        } else if (checkExitStatus) {
            query = "INSERT INTO goods VALUES ( null, '" + jName.getText() + "','"
                    + jHPB.getText() + "','" + jHPP.getText() + "','" + jStock.getText() + "')";
            connection.exQuery(query);
        }

        dispose();
    }

    public boolean run() {
        this.setVisible(true);
        return checkExitStatus;
    }

    private void initComponents() {

        jLabelHeader = new javax.swing.JLabel();
        jLabel1 = new javax.swing.JLabel();
        jLabel2 = new javax.swing.JLabel();
        jLabel3 = new javax.swing.JLabel();
        jLabel4 = new javax.swing.JLabel();
        jName = new javax.swing.JTextField();
        jHPB = new javax.swing.JTextField();
        jHPP = new javax.swing.JTextField();
        jStock = new javax.swing.JTextField();
        jButtonCancel = new javax.swing.JButton();
        jButtonOk = new javax.swing.JButton();

        jLabelHeader.setFont(new java.awt.Font("Roboto", 1, 24)); // NOI18N
        jLabelHeader.setHorizontalAlignment(javax.swing.SwingConstants.CENTER);
        jLabelHeader.setText("Goods Manager");

        jLabel1.setText("Goods          ");

        jLabel2.setText("HPB");

        jLabel3.setText("HPP");

        jLabel4.setText("Stock");

        jButtonCancel.setText("Cancel");
        jButtonCancel.setActionCommand("jButtonCancel");
        jButtonCancel.addActionListener(this);

        jButtonOk.setText("Ok");
        jButtonOk.setActionCommand("jButtonOk");
        jButtonOk.addActionListener(this);

        javax.swing.GroupLayout layout = new javax.swing.GroupLayout(getContentPane());
        getContentPane().setLayout(layout);
        layout.setHorizontalGroup(
                layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
                        .addGroup(layout.createSequentialGroup()
                                .addContainerGap()
                                .addGroup(layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
                                        .addComponent(jLabelHeader, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, Short.MAX_VALUE)
                                        .addGroup(layout.createSequentialGroup()
                                                .addComponent(jLabel4)
                                                .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.RELATED, javax.swing.GroupLayout.DEFAULT_SIZE, Short.MAX_VALUE)
                                                .addComponent(jStock, javax.swing.GroupLayout.PREFERRED_SIZE, 221, javax.swing.GroupLayout.PREFERRED_SIZE))
                                        .addGroup(javax.swing.GroupLayout.Alignment.TRAILING, layout.createSequentialGroup()
                                                .addGap(0, 0, Short.MAX_VALUE)
                                                .addComponent(jButtonOk, javax.swing.GroupLayout.PREFERRED_SIZE, 59, javax.swing.GroupLayout.PREFERRED_SIZE)
                                                .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.UNRELATED)
                                                .addComponent(jButtonCancel))
                                        .addGroup(layout.createSequentialGroup()
                                                .addGroup(layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
                                                        .addGroup(layout.createSequentialGroup()
                                                                .addComponent(jLabel1)
                                                                .addGap(18, 18, 18)
                                                                .addComponent(jName, javax.swing.GroupLayout.PREFERRED_SIZE, 221, javax.swing.GroupLayout.PREFERRED_SIZE))
                                                        .addGroup(layout.createSequentialGroup()
                                                                .addComponent(jLabel2)
                                                                .addGap(18, 18, 18)
                                                                .addComponent(jHPB, javax.swing.GroupLayout.PREFERRED_SIZE, 221, javax.swing.GroupLayout.PREFERRED_SIZE))
                                                        .addGroup(layout.createSequentialGroup()
                                                                .addComponent(jLabel3)
                                                                .addGap(18, 18, 18)
                                                                .addComponent(jHPP, javax.swing.GroupLayout.PREFERRED_SIZE, 221, javax.swing.GroupLayout.PREFERRED_SIZE)))
                                                .addGap(0, 0, Short.MAX_VALUE)))
                                .addContainerGap())
        );

        layout.linkSize(javax.swing.SwingConstants.HORIZONTAL, new java.awt.Component[]{jLabel1, jLabel2, jLabel3, jLabel4});

        layout.linkSize(javax.swing.SwingConstants.HORIZONTAL, new java.awt.Component[]{jHPB, jHPP, jName, jStock});

        layout.linkSize(javax.swing.SwingConstants.HORIZONTAL, new java.awt.Component[]{jButtonCancel, jButtonOk});

        layout.setVerticalGroup(
                layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
                        .addGroup(layout.createSequentialGroup()
                                .addContainerGap()
                                .addComponent(jLabelHeader, javax.swing.GroupLayout.PREFERRED_SIZE, 55, javax.swing.GroupLayout.PREFERRED_SIZE)
                                .addGap(18, 18, 18)
                                .addGroup(layout.createParallelGroup(javax.swing.GroupLayout.Alignment.BASELINE)
                                        .addComponent(jLabel1)
                                        .addComponent(jName, javax.swing.GroupLayout.PREFERRED_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.PREFERRED_SIZE))
                                .addGap(18, 18, 18)
                                .addGroup(layout.createParallelGroup(javax.swing.GroupLayout.Alignment.BASELINE)
                                        .addComponent(jLabel2)
                                        .addComponent(jHPB, javax.swing.GroupLayout.PREFERRED_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.PREFERRED_SIZE))
                                .addGap(18, 18, 18)
                                .addGroup(layout.createParallelGroup(javax.swing.GroupLayout.Alignment.BASELINE)
                                        .addComponent(jLabel3)
                                        .addComponent(jHPP, javax.swing.GroupLayout.PREFERRED_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.PREFERRED_SIZE))
                                .addGap(18, 18, 18)
                                .addGroup(layout.createParallelGroup(javax.swing.GroupLayout.Alignment.BASELINE)
                                        .addComponent(jLabel4)
                                        .addComponent(jStock, javax.swing.GroupLayout.PREFERRED_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.PREFERRED_SIZE))
                                .addGap(18, 18, 18)
                                .addGroup(layout.createParallelGroup(javax.swing.GroupLayout.Alignment.BASELINE)
                                        .addComponent(jButtonCancel)
                                        .addComponent(jButtonOk))
                                .addContainerGap(javax.swing.GroupLayout.DEFAULT_SIZE, Short.MAX_VALUE))
        );

        layout.linkSize(javax.swing.SwingConstants.VERTICAL, new java.awt.Component[]{jLabel1, jLabel2, jLabel3, jLabel4});

        layout.linkSize(javax.swing.SwingConstants.VERTICAL, new java.awt.Component[]{jHPB, jHPP, jName, jStock});

        layout.linkSize(javax.swing.SwingConstants.VERTICAL, new java.awt.Component[]{jButtonCancel, jButtonOk});

        pack();
    }

    private javax.swing.JButton jButtonCancel;
    private javax.swing.JButton jButtonOk;
    private javax.swing.JTextField jHPB;
    private javax.swing.JTextField jHPP;
    private javax.swing.JLabel jLabel1;
    private javax.swing.JLabel jLabel2;
    private javax.swing.JLabel jLabel3;
    private javax.swing.JLabel jLabel4;
    private javax.swing.JLabel jLabelHeader;
    private javax.swing.JTextField jName;
    private javax.swing.JTextField jStock;
}
