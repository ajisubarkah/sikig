package goodsinventory.frames;

import goodsinventory.utility.FunctionUtility;
import java.awt.Frame;
import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;
import javax.swing.JDialog;

public class PaymentActivity extends JDialog implements ActionListener {

    private final int totalHarga;
    boolean checkExitStatus = false;
    FunctionUtility function = new FunctionUtility();

    public PaymentActivity(Frame parent, String title, int hargaTotal) {
        super(parent, title, true);
        this.totalHarga = hargaTotal;
        initComponents();
        setResizable(false);
        setLocationRelativeTo(null);
    }

    @Override
    public void actionPerformed(ActionEvent ae) {
        String temp = ae.getActionCommand();

        if (temp.equals("btnOk")) {
            checkExitStatus = true;
            dispose();
        }
        if (temp.equals("btnCancel")) {
            dispose();
        }

        if (temp.equals("btnClear")) {
            jBayar.setText("Rp. 0,00");
            jKembalian.setText("Rp. -" + function.moneyFormat(totalHarga) + ",00");
            jButton12.setEnabled(false);
        }

        if (temp.equals("btnDelete")) {
            if (!jBayar.getText().equals("Rp. 0,00")) {
                String str = function.moneyFormat(jBayar.getText());
                if (str.length() <= 1) {
                    jBayar.setText("Rp. 0,00");
                    jKembalian.setText("Rp. -" + function.moneyFormat(totalHarga) + ",00");
                } else {
                    int newBayar = Integer.parseInt(str.substring(0, str.length() - 1));
                    jBayar.setText("Rp. " + function.moneyFormat(newBayar) + ",00");
                    int newKembalian = Integer.parseInt(function.moneyFormat(jBayar.getText())) - Integer.parseInt(function.moneyFormat(jTotal.getText()));
                    jKembalian.setText("Rp. " + function.moneyFormat(newKembalian) + ",00");
                    if (newKembalian < 0) {
                        jButton12.setEnabled(false);
                    }
                }
            }
        }

        for (int i = 0; i <= 9; i++) {
            if (temp.equals(i + "")) {
                setBayar(i + "");
            }
        }
    }

    private void setBayar(String quantity) {
        if ("Rp. 0,00".equals(jBayar.getText())) {
            jBayar.setText("Rp. " + function.moneyFormat(Integer.parseInt(quantity)) + ",00");
            setKembalian(jBayar.getText());
        } else {
            String temp = function.moneyFormat(jBayar.getText()) + quantity;
            int tempFormat = Integer.parseInt(temp);
            jBayar.setText("Rp. " + function.moneyFormat(tempFormat) + ",00");
            setKembalian(jBayar.getText());
        }
    }

    private void setKembalian(String bayar) {
        int harga = Integer.parseInt(function.moneyFormat(jTotal.getText()));
        int kembalian = Integer.parseInt(function.moneyFormat(bayar)) - harga;
        jKembalian.setText("Rp. " + function.moneyFormat(kembalian) + ",00");
        if (kembalian >= 0) {
            jButton12.setEnabled(true);
        }
    }

    public boolean run() {
        this.setVisible(true);
        return checkExitStatus;
    }

    private void initComponents() {

        jLabel1 = new javax.swing.JLabel();
        jTotal = new javax.swing.JTextField();
        jTotal.setText("Rp. " + function.moneyFormat(totalHarga) + ",00");
        jTotal.setEditable(false);
        jBayar = new javax.swing.JTextField("Rp. 0,00");
        jBayar.setEditable(false);
        jLabel2 = new javax.swing.JLabel();
        jLabel3 = new javax.swing.JLabel();
        jKembalian = new javax.swing.JTextField();
        jKembalian.setText("Rp. -" + function.moneyFormat(totalHarga) + ",00");
        jKembalian.setEditable(false);
        jButton1 = new javax.swing.JButton();
        jButton2 = new javax.swing.JButton();
        jButton3 = new javax.swing.JButton();
        jButton4 = new javax.swing.JButton();
        jButton5 = new javax.swing.JButton();
        jButton6 = new javax.swing.JButton();
        jButton7 = new javax.swing.JButton();
        jButton8 = new javax.swing.JButton();
        jButton9 = new javax.swing.JButton();
        jButton10 = new javax.swing.JButton();
        jButtonBack = new javax.swing.JButton();
        jButton11 = new javax.swing.JButton();
        jButton12 = new javax.swing.JButton();
        jButton13 = new javax.swing.JButton();

        jLabel1.setText("Total Harga");

        jLabel2.setText("Total Bayar");

        jLabel3.setFont(new java.awt.Font("Roboto", 1, 16)); // NOI18N
        jLabel3.setText("Kembalian");

        jKembalian.setFont(new java.awt.Font("Roboto", 1, 16)); // NOI18N
        jButton1.setText("1");
        jButton1.setActionCommand("1");
        jButton1.addActionListener(this);

        jButton2.setText("2");
        jButton2.setActionCommand("2");
        jButton2.addActionListener(this);

        jButton3.setText("3");
        jButton3.setActionCommand("3");
        jButton3.addActionListener(this);

        jButton4.setText("4");
        jButton4.setActionCommand("4");
        jButton4.addActionListener(this);

        jButton5.setText("5");
        jButton5.setActionCommand("5");
        jButton5.addActionListener(this);

        jButton6.setText("6");
        jButton6.setActionCommand("6");
        jButton6.addActionListener(this);

        jButton7.setText("7");
        jButton7.setActionCommand("7");
        jButton7.addActionListener(this);

        jButton8.setText("8");
        jButton8.setActionCommand("8");
        jButton8.addActionListener(this);

        jButton9.setText("9");
        jButton9.setActionCommand("1");
        jButton9.addActionListener(this);

        jButton10.setText("0");
        jButton10.setActionCommand("0");
        jButton10.addActionListener(this);

        jButtonBack.setText("C");
        jButtonBack.setActionCommand("btnClear");
        jButtonBack.addActionListener(this);

        jButton11.setText("<");
        jButton11.setActionCommand("btnDelete");
        jButton11.addActionListener(this);

        jButton12.setText("OK");
        jButton12.setActionCommand("btnOk");
        jButton12.addActionListener(this);
        jButton12.setEnabled(false);

        jButton13.setText("Cancel");
        jButton13.setActionCommand("btnCancel");
        jButton13.addActionListener(this);

        javax.swing.GroupLayout layout = new javax.swing.GroupLayout(getContentPane());
        getContentPane().setLayout(layout);
        layout.setHorizontalGroup(
                layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
                        .addGroup(layout.createSequentialGroup()
                                .addContainerGap()
                                .addGroup(layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING, false)
                                        .addGroup(layout.createSequentialGroup()
                                                .addGroup(layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
                                                        .addComponent(jLabel1)
                                                        .addComponent(jLabel2))
                                                .addGroup(layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
                                                        .addGroup(layout.createSequentialGroup()
                                                                .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.RELATED, javax.swing.GroupLayout.DEFAULT_SIZE, Short.MAX_VALUE)
                                                                .addComponent(jTotal, javax.swing.GroupLayout.PREFERRED_SIZE, 200, javax.swing.GroupLayout.PREFERRED_SIZE))
                                                        .addGroup(layout.createSequentialGroup()
                                                                .addGap(29, 29, 29)
                                                                .addComponent(jBayar, javax.swing.GroupLayout.PREFERRED_SIZE, 200, javax.swing.GroupLayout.PREFERRED_SIZE)
                                                                .addGap(0, 0, Short.MAX_VALUE))))
                                        .addGroup(layout.createSequentialGroup()
                                                .addComponent(jLabel3)
                                                .addGap(18, 18, 18)
                                                .addComponent(jKembalian, javax.swing.GroupLayout.PREFERRED_SIZE, 202, javax.swing.GroupLayout.PREFERRED_SIZE))
                                        .addGroup(layout.createSequentialGroup()
                                                .addComponent(jButton12, javax.swing.GroupLayout.PREFERRED_SIZE, 211, javax.swing.GroupLayout.PREFERRED_SIZE)
                                                .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.RELATED)
                                                .addComponent(jButton13, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, Short.MAX_VALUE)))
                                .addGap(18, 18, 18)
                                .addGroup(layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
                                        .addGroup(layout.createSequentialGroup()
                                                .addComponent(jButton1, javax.swing.GroupLayout.PREFERRED_SIZE, 32, javax.swing.GroupLayout.PREFERRED_SIZE)
                                                .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.RELATED)
                                                .addComponent(jButton2, javax.swing.GroupLayout.PREFERRED_SIZE, 32, javax.swing.GroupLayout.PREFERRED_SIZE)
                                                .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.RELATED)
                                                .addComponent(jButton3, javax.swing.GroupLayout.PREFERRED_SIZE, 32, javax.swing.GroupLayout.PREFERRED_SIZE))
                                        .addGroup(layout.createSequentialGroup()
                                                .addComponent(jButton4, javax.swing.GroupLayout.PREFERRED_SIZE, 32, javax.swing.GroupLayout.PREFERRED_SIZE)
                                                .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.RELATED)
                                                .addComponent(jButton5, javax.swing.GroupLayout.PREFERRED_SIZE, 32, javax.swing.GroupLayout.PREFERRED_SIZE)
                                                .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.RELATED)
                                                .addComponent(jButton6, javax.swing.GroupLayout.PREFERRED_SIZE, 32, javax.swing.GroupLayout.PREFERRED_SIZE))
                                        .addGroup(layout.createSequentialGroup()
                                                .addGroup(layout.createParallelGroup(javax.swing.GroupLayout.Alignment.TRAILING, false)
                                                        .addGroup(layout.createSequentialGroup()
                                                                .addComponent(jButton11, javax.swing.GroupLayout.PREFERRED_SIZE, 32, javax.swing.GroupLayout.PREFERRED_SIZE)
                                                                .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.RELATED, javax.swing.GroupLayout.DEFAULT_SIZE, Short.MAX_VALUE)
                                                                .addComponent(jButton10, javax.swing.GroupLayout.PREFERRED_SIZE, 32, javax.swing.GroupLayout.PREFERRED_SIZE))
                                                        .addGroup(layout.createSequentialGroup()
                                                                .addComponent(jButton7, javax.swing.GroupLayout.PREFERRED_SIZE, 32, javax.swing.GroupLayout.PREFERRED_SIZE)
                                                                .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.RELATED)
                                                                .addComponent(jButton8, javax.swing.GroupLayout.PREFERRED_SIZE, 32, javax.swing.GroupLayout.PREFERRED_SIZE)))
                                                .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.RELATED)
                                                .addGroup(layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
                                                        .addComponent(jButton9, javax.swing.GroupLayout.PREFERRED_SIZE, 32, javax.swing.GroupLayout.PREFERRED_SIZE)
                                                        .addComponent(jButtonBack, javax.swing.GroupLayout.PREFERRED_SIZE, 32, javax.swing.GroupLayout.PREFERRED_SIZE))))
                                .addContainerGap(javax.swing.GroupLayout.DEFAULT_SIZE, Short.MAX_VALUE))
        );

        layout.linkSize(javax.swing.SwingConstants.HORIZONTAL, new java.awt.Component[]{jBayar, jKembalian, jTotal});

        layout.setVerticalGroup(
                layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
                        .addGroup(layout.createSequentialGroup()
                                .addContainerGap()
                                .addGroup(layout.createParallelGroup(javax.swing.GroupLayout.Alignment.BASELINE)
                                        .addComponent(jLabel1)
                                        .addComponent(jTotal, javax.swing.GroupLayout.PREFERRED_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.PREFERRED_SIZE)
                                        .addComponent(jButton1, javax.swing.GroupLayout.PREFERRED_SIZE, 30, javax.swing.GroupLayout.PREFERRED_SIZE)
                                        .addComponent(jButton2, javax.swing.GroupLayout.PREFERRED_SIZE, 30, javax.swing.GroupLayout.PREFERRED_SIZE)
                                        .addComponent(jButton3, javax.swing.GroupLayout.PREFERRED_SIZE, 30, javax.swing.GroupLayout.PREFERRED_SIZE))
                                .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.RELATED)
                                .addGroup(layout.createParallelGroup(javax.swing.GroupLayout.Alignment.BASELINE)
                                        .addComponent(jBayar, javax.swing.GroupLayout.PREFERRED_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.PREFERRED_SIZE)
                                        .addComponent(jLabel2)
                                        .addComponent(jButton4, javax.swing.GroupLayout.PREFERRED_SIZE, 30, javax.swing.GroupLayout.PREFERRED_SIZE)
                                        .addComponent(jButton5, javax.swing.GroupLayout.PREFERRED_SIZE, 30, javax.swing.GroupLayout.PREFERRED_SIZE)
                                        .addComponent(jButton6, javax.swing.GroupLayout.PREFERRED_SIZE, 30, javax.swing.GroupLayout.PREFERRED_SIZE))
                                .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.RELATED)
                                .addGroup(layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
                                        .addGroup(layout.createSequentialGroup()
                                                .addGroup(layout.createParallelGroup(javax.swing.GroupLayout.Alignment.BASELINE)
                                                        .addComponent(jButton7, javax.swing.GroupLayout.PREFERRED_SIZE, 30, javax.swing.GroupLayout.PREFERRED_SIZE)
                                                        .addComponent(jButton8, javax.swing.GroupLayout.PREFERRED_SIZE, 30, javax.swing.GroupLayout.PREFERRED_SIZE)
                                                        .addComponent(jButton9, javax.swing.GroupLayout.PREFERRED_SIZE, 30, javax.swing.GroupLayout.PREFERRED_SIZE))
                                                .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.RELATED)
                                                .addGroup(layout.createParallelGroup(javax.swing.GroupLayout.Alignment.BASELINE)
                                                        .addComponent(jButton10, javax.swing.GroupLayout.PREFERRED_SIZE, 30, javax.swing.GroupLayout.PREFERRED_SIZE)
                                                        .addComponent(jButtonBack, javax.swing.GroupLayout.PREFERRED_SIZE, 30, javax.swing.GroupLayout.PREFERRED_SIZE)
                                                        .addComponent(jButton11, javax.swing.GroupLayout.PREFERRED_SIZE, 30, javax.swing.GroupLayout.PREFERRED_SIZE))
                                                .addContainerGap(javax.swing.GroupLayout.DEFAULT_SIZE, Short.MAX_VALUE))
                                        .addGroup(layout.createSequentialGroup()
                                                .addGroup(layout.createParallelGroup(javax.swing.GroupLayout.Alignment.BASELINE)
                                                        .addComponent(jKembalian)
                                                        .addComponent(jLabel3))
                                                .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.RELATED)
                                                .addGroup(layout.createParallelGroup(javax.swing.GroupLayout.Alignment.BASELINE)
                                                        .addComponent(jButton12)
                                                        .addComponent(jButton13))
                                                .addContainerGap())))
        );

        pack();
    }

    private javax.swing.JTextField jBayar;
    private javax.swing.JTextField jKembalian;
    private javax.swing.JButton jButton1;
    private javax.swing.JButton jButton10;
    private javax.swing.JButton jButton11;
    private javax.swing.JButton jButton12;
    private javax.swing.JButton jButton13;
    private javax.swing.JButton jButton2;
    private javax.swing.JButton jButton3;
    private javax.swing.JButton jButton4;
    private javax.swing.JButton jButton5;
    private javax.swing.JButton jButton6;
    private javax.swing.JButton jButton7;
    private javax.swing.JButton jButton8;
    private javax.swing.JButton jButton9;
    private javax.swing.JButton jButtonBack;
    private javax.swing.JLabel jLabel1;
    private javax.swing.JLabel jLabel2;
    private javax.swing.JLabel jLabel3;
    private javax.swing.JTextField jTotal;
}
