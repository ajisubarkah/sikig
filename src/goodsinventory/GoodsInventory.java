package goodsinventory;

import goodsinventory.frames.StartupActivity;
import javax.swing.UIManager;
import javax.swing.UnsupportedLookAndFeelException;

public class GoodsInventory {

    public static void main(String[] args) {        
        try {
//            UIManager.setLookAndFeel("com.sun.java.swing.plaf.nimbus.NimbusLookAndFeel");
            UIManager.setLookAndFeel("com.sun.java.swing.plaf.gtk.GTKLookAndFeel");
            new StartupActivity();
        } catch (ClassNotFoundException | IllegalAccessException | InstantiationException | UnsupportedLookAndFeelException e) {
            javax.swing.JOptionPane.showMessageDialog(null, "Application Error!\nFor further information contact administrator!", "Error", javax.swing.JOptionPane.ERROR_MESSAGE);
        }
    }

}
